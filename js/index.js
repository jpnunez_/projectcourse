$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#contacto').on('show.bs.modal', function(e){
      console.log('hola');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-danger');
      $('#contactoBtn').prop('disabled',true);

    });
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('todo bien?');
    });
    $('#contacto').on('hide.bs.modal', function(e){
      console.log('chau');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('nos vemos');
      $('#contactoBtn').prop('disabled',false);
      $('#contactoBtn').removeClass('btn-danger');
      $('#contactoBtn').addClass('btn-outline-success');
    });
});
